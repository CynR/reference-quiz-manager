package com.zenika.academy.quiz.questions;

import java.util.Random;

public class ListMovies {

    String movieName;

    public ListMovies(String movieName) {

        this.movieName = movieName;
    }


    public String getMovieName() {
        return movieName;
    }
}
