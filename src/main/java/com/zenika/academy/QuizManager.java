package com.zenika.academy;
import com.zenika.academy.quiz.questions.*;
import com.zenika.academy.quiz.Player;
import com.zenika.academy.tmdb.TmdbClient;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class QuizManager {
    public static void main(String[] args) throws IOException {
        File f = new File("src/main/resources/tmdb-api-key.txt");
        TmdbClient tmbclient = new TmdbClient(f);
        Generator g = Generator.getGeneratorInstance(tmbclient);
        Scanner sc = new Scanner(System.in);
        Player p = createPlayer(sc);
        //list where the questions generated are stocked to prevent ask the user one that has already been asked
        List<String> listExistingQuestions = new ArrayList<>();
        int scoreTotalIfAllAnswersWereCorrect = 0;

            for (int numberRounds = 0; numberRounds < 3; numberRounds++) {
                Question questionDif =
                        g.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.DIFFICILE).get();
                if (!listExistingQuestions.contains(questionDif.getDisplayableText())) {
                    p.scorePoints(askQuestion(sc,questionDif));
                    scoreTotalIfAllAnswersWereCorrect = scoreTotalIfAllAnswersWereCorrect +
                            ((g.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.DIFFICILE).get().getPoints())*2);
                    listExistingQuestions.add(questionDif.getDisplayableText());
                }
                Question questionFac =
                        g.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.FACILE).get();
                if (!listExistingQuestions.contains(questionFac.getDisplayableText())) {
                    p.scorePoints(askQuestion(sc,questionFac));
                    scoreTotalIfAllAnswersWereCorrect = scoreTotalIfAllAnswersWereCorrect +
                            ((g.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.FACILE).get().getPoints())*2);
                    listExistingQuestions.add(questionFac.getDisplayableText());
                }
                Question questionMoy =
                        g.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.MOYENNE).get();
                if (!listExistingQuestions.contains(questionMoy.getJustQuestionText())) {
                    p.scorePoints(askQuestion(sc,questionMoy));
                    scoreTotalIfAllAnswersWereCorrect = scoreTotalIfAllAnswersWereCorrect +
                            ((g.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.MOYENNE).get().getPoints())*2);
                    listExistingQuestions.add(questionMoy.getJustQuestionText());
                }
            }
        System.out.println(p.congratulations());
        System.out.println("Le score maximum a été de " + scoreTotalIfAllAnswersWereCorrect + " points");
    }

    private static int askQuestion(Scanner sc, Question q) {
        System.out.println(q.getDisplayableText());
        String userAnswer = sc.nextLine();
        switch (q.tryAnswer(userAnswer)) {
            case CORRECT:
                return (2*q.getPoints());
            case ALMOST_CORRECT:
                return (q.getPoints());
            case INCORRECT:
            default:
                return 0;
        }
    }

    private static Player createPlayer(Scanner sc) {
        System.out.println("Quel est votre nom ?");
        String userName = sc.nextLine();
        return new Player(userName);
    }

}
