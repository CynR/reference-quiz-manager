package com.zenika.academy.quiz.questions;
import com.zenika.academy.quiz.Player;
import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.IOException;
import java.time.Year;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class GeneratorTest {
    @Test
    void doesTheQuestionIsGenerate() throws IOException {
        //File f = new File("src/main/resources/tmdb-api-key.txt");
        MovieInfo info = new MovieInfo("Interstellar", Year.of(2014), List.of(
                new MovieInfo.Character("Joseph \"Coop\" Cooper", "Matthew McConaughey"),
                new MovieInfo.Character("Murphy \"Murph\" Cooper", "Jessica Chastain"),
                new MovieInfo.Character("Dr. Amelia Brand", "Anne Hathaway"),
                new MovieInfo.Character("Professor John Brand", "Michael Caine"),
                new MovieInfo.Character("Tom Cooper", "Casey Affleck")
        ));
        TmdbClient mocktmbclient = mock(TmdbClient.class);
        when(mocktmbclient.getMovie("Interstellar")).thenReturn(Optional.of(info));
        Generator g = Generator.getGeneratorInstance(mocktmbclient);

        int numberOfActors = 3;
        String actors = "";
        for(int character = 0; character < numberOfActors; character++){
            actors = actors + info.cast.get(character).actorName + ", ";
        }
        String tBefore = "Quel film est sorti dans l'année " + info.year + " qui a comme cast: " + actors;
        int len = tBefore.length();
        String textQTitle = tBefore.substring(0,len-2) + " ?";

        Assertions.assertEquals(textQTitle,
                (g.generate("Interstellar", Generator.DifficultyLevel.DIFFICILE).get()).getDisplayableText());
        assertTrue((g.generate("Interstellar", Generator.DifficultyLevel.DIFFICILE)).isPresent());

    }

    @Test
    void doesTheQuestionIsGenerateWithoutGivingAMovieTitle() throws IOException {

        TmdbClient mocktmbclient = mock(TmdbClient.class);
        when(mocktmbclient.getMovie("Interstellar")).thenReturn(Optional.empty());
        Generator g = Generator.getGeneratorInstance(mocktmbclient);

        Assertions.assertTrue((g.generate("Interstellar", Generator.DifficultyLevel.DIFFICILE)).isPresent());
    }

    @Test
    void doesItGenerateJusteOneInstance() throws IOException {
        File f = new File("src/main/resources/tmdb-api-key.txt");
        TmdbClient tmbclient = new TmdbClient(f);
        Generator g = Generator.getGeneratorInstance(tmbclient);
        Generator g2 = Generator.getGeneratorInstance(tmbclient);
        Generator g3 = Generator.getGeneratorInstance(tmbclient);

        Assertions.assertTrue(g == g2 &&
                g2 == g3);
    }

}
