package com.zenika.academy.quiz.questions;

import org.junit.jupiter.api.Test;

import static com.zenika.academy.quiz.questions.AnswerResult.CORRECT;
import static com.zenika.academy.quiz.questions.AnswerResult.INCORRECT;
import static org.junit.jupiter.api.Assertions.*;

class OpenQuestionTest {

    @Test
    void getDisplayableText() {
        Question q = new OpenQuestion("Qui marche de travers ?", "C'est le crabe tout vert",3);

        assertEquals("Qui marche de travers ?", q.getDisplayableText());
    }

    @Test
    void tryCorrectAnswer() {
        Question q = new OpenQuestion("Qui marche de travers ?", "C'est le crabe tout vert",3);

        assertEquals(CORRECT, q.tryAnswer("C'est le crabe tout vert"));
    }

    @Test
    void tryIncorrectAnswer() {
        Question q = new OpenQuestion("Qui marche de travers ?", "C'est le crabe tout vert",3);

        assertEquals(INCORRECT, q.tryAnswer("Un homme en état d'ébriété"));
    }

    @Test
    void tryAlmostCorrectAnswer() {
        Question q = new OpenQuestion("Qui marche de travers ?", "C'est le crabe tout vert",3);

        assertEquals(INCORRECT, q.tryAnswer("C'est le crbe tout vart"));
    }
}